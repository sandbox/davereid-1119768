<?php

/**
 * @file
 * UUID callbacks for the uuid module.
 *
 * @ingroup uuid
 */

/**
 * Implements hook_uuid_generator_info().
 */
function uuid_uuid_generator_info() {
  $info = array();

  // PECL and OSSP are 2 incompatiable implementations in PHP that use the same namespace.
  if (function_exists('uuid_create') && !function_exists('uuid_make')) {
    $info['pecl'] = array(
      'label' => t('PECL Extension'),
      'callback' => 'uuid_generate_pecl',
    );
  }

  if (Database::getConnection()->databaseType() == 'mysql') {
    $info['mysql'] = array(
      'label' => t('MySQL - SELECT UUID()'),
      'callback' => 'uuid_generate_mysql',
    );
  }

  if (function_exists('com_create_guid')) {
    $info['com'] = array(
      'label' => t('Windows COM'),
      'callback' => 'uuid_generate_com',
    );
  }

  $info['php'] = array(
    'label' => t('PHP Fallback'),
    'callback' => 'uuid_generate_php',
  );

  foreach ($info as &$generator) {
    $generator['include'] = drupal_get_path('module', 'uuid') . '/uuid.uuid.inc';
  }

  return $info;
}

/**
 * Generates a UUID using the Windows internal GUID generator.
 *
 * See http://php.net/com_create_guid
 */
function uuid_generate_com() {
  // remove {} wrapper and make lower case to keep result consistent
  return strtolower(trim(com_create_guid(), '{}'));
}

/**
 * Generates a UUID using MySQL's implementation.
 *
 * See http://dev.mysql.com/doc/refman/5.0/en/miscellaneous-functions.html#function_uuid
 */
function uuid_generate_mysql() {
  return db_query('SELECT UUID()')->fetchField();
}

/**
 * Generates a UUID using the PECL extension.
 *
 * See http://pecl.php.net/package/uuid
 */
function uuid_generate_pecl() {
  return uuid_create(UUID_TYPE_DEFAULT);
}

/**
 * Generates a UUID v4 using PHP code.
 *
 * See http://php.net/uniqid#65879
 */
function uuid_generate_php() {
  // The field names refer to RFC 4122 section 4.1.2.
  return sprintf('%04x%04x-%04x-%03x4-%04x-%04x%04x%04x',
    // 32 bits for "time_low".
    mt_rand(0, 65535), mt_rand(0, 65535),
    // 16 bits for "time_mid".
    mt_rand(0, 65535),
    // 12 bits before the 0100 of (version) 4 for "time_hi_and_version".
    mt_rand(0, 4095),
    bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)),
    // 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
    // (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
    // 8 bits for "clk_seq_low" 48 bits for "node".
    mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)
  );
}
