<?php

/**
 * @file
 * Hooks provided by the UUID module.
 *
 * @ingroup uuid
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provide information about available UUID generators.
 *
 * @see hook_uuid_generator_info_alter().
 */
function hook_uuid_generator_info() {
  $info['kitten'] = array(
    'label' => 'Kitten UUID generator',
    'callback' => 'uuid_generate_kitten',
    'include' => drupal_get_path('module', 'kitten') . '/kitten.uuid.inc',
  );

  return $info;
}

/**
 * Alter UUID generator info.
 *
 * @see hook_uuid_generator_info().
 */
function hook_uuid_generator_info_alter(&$info) {
  // Change the kitten generator into the cat generator.
  $info['kitten']['label'] = 'Cat UUID generator';
  $info['kitten']['callback'] = 'uuid_generate_cat';
  $info['kitten']['include'] = drupal_get_path('module', 'kitten') . '/cat.uuid.inc';
}

/**
 * @} End of "addtogroup hooks".
 */
