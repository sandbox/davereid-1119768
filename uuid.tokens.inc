<?php

/**
 * @file
 * Token callbacks for the uuid module.
 *
 * @ingroup uuid
 */

/**
 * Implements hook_token_info().
 */
function uuid_token_info() {
  $info = array();

  if (!module_exists('token')) {
    // We depend on the token_get_entity_mapping() function in token.module.
    return $info;
  }

  // Random tokens.
  $info['tokens']['random']['uuid'] = array(
    'name' => t('UUID'),
    'description' => t('A random, newly generated UUID.'),
  );

  // Add [entity:uuid] tokens for all entities.
  $entities = entity_get_info();
  foreach ($entities as $entity => $entity_info) {
    if (!isset($entity_info['token type'])) {
      continue;
    }
    if (!uuid_entity_supports_uuid($entity)) {
      // If the entity type does not support UUIDs, do not add a token.
      continue;
    }

    $tokens = &$info['tokens'][$entity_info['token type']];
    $tokens['uuid'] = array(
      'name' => t('UUID'),
      'description' => t('The universally unique identifier of the @entity.', array('@entity' => drupal_strtolower($entity_info['label']))),
    );
    $tokens['uuid-url'] = array(
      'name' => t('UUID URL'),
      'description' => t('The universally unique identifier URL of the @entity.', array('@entity' => drupal_strtolower($entity_info['label']))),
      'type' => 'url',
    );
  }

  return $info;
}

/**
 * Implements hook_tokens().
 */
function uuid_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if (!module_exists('token')) {
    // We depend on the token_get_entity_mapping() function in token.module.
    return $replacements;
  }

  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);

  // Random tokens.
  if ($type == 'random') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'uuid':
          $replacements[$original] = uuid_uuid_generate();
          break;
      }
    }
  }

  // Entity tokens.
  if (!empty($data[$type]) && ($entity_type = token_get_entity_mapping('token', $type)) && uuid_entity_supports_uuid($entity_type) && !empty($data[$type]->uuid)) {
    $entity = $data[$type];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'uuid':
          $replacements[$original] = $sanitize ? check_plain($entity->uuid) : $entity->uuid;
          break;
        case 'uuid-url':
          $replacements[$original] = url("uuid/{$entity->uuid}", $url_options);
          break;
      }
    }

    // Chained token relationships.
    if ($url_tokens = token_find_with_prefix($tokens, 'uuid-url')) {
      $replacements += token_generate('url', $url_tokens, array('path' => "uuid/{$entity->uuid}"), $options);
    }
  }

  return $replacements;
}
