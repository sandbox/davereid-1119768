<?php

/**
 * @file
 * Views integration and data for the uuid module.
 */

/**
 * Implements hook_views_data().
 */
function uuid_views_data() {
  // Basic table information.
  $data['uuid']['table']['group'] = t('UUID');

  // Add joins for all other entity types.
  $entity_tables = array();
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (empty($entity_info['base table']) || empty($entity_info['entity keys']['id'])) {
      continue;
    }

    $data['uuid']['table']['join'][$entity_info['base table']] = array(
      'type' => 'INNER',
      'left_field' => $entity_info['entity keys']['id'],
      'field' => 'entity_id',
      'extra' => array(
        array('field' => 'entity_type', 'value' => $entity_type),
      ),
    );
  }

  // {uuid}.uuid
  $data['uuid']['uuid'] = array(
    'title' => t('UUID'),
    'help' => t('The universally unique identifier for the entity.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
